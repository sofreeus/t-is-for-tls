## DRY RUN  
If you want to test "renew" or "certonly" without saving any certs to disk, add `--dry-run` flag.   
This will test command validity, for when the cert is will get created.
