## TESTING AGAINST THE STAGING ENV 
Letsencrypt has an environment for testing certificate creation and renewal, that won't count against your rate limit. (https://letsencrypt.org/docs/rate-limits/)  

These are not usable certificates, as they are not from Letsencrypt's root account, and not recognized by any OS or browser.  They are meant to test scripting and workflows. (https://letsencrypt.org/docs/staging-environment/)  

Use the `--test-cert` flag to test "renew" and "certonly" including actual certificate creation.   
