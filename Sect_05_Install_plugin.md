## INSTALL CERTBOT PLUGIN(s)  

&nbsp;  `pip install certbot-plugin-gandi`  &nbsp; &nbsp; &nbsp;  install certbot plugin that will handle DNS challenge for letsencrypt   
&nbsp;  `certbot plugins --text`  &nbsp; &nbsp; &nbsp;  see certbot componenets, now including the **gandi** plugin  


&nbsp;  &nbsp;  _plugins use the DNS provider's API, and so you'll need an API token, that the certbot DNS provider plugin will use in an .ini file._    


There are many DNS providers from the official certbot site, like Amazon, Cloudflare, Digital Ocean, Google, IBM, etc.  
 (https://community.letsencrypt.org/t/dns-providers-who-easily-integrate-with-lets-encrypt-dns-validation/86438).   

There are also approved 3rd party [plugins](https://eff-certbot.readthedocs.io/en/stable/using.html#third-party-plugins) for other DNS providers, and **gandi** is one of those.  In this case, gandi documentation suggests this particular plugin.   
https://github.com/obynio/certbot-plugin-gandi   


&nbsp; &nbsp;  _note: I think certbot/LE are no longer supporting 3rd party plugins_ 

<br/><br/>   
