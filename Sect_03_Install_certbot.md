## PIP INSTALL CERTBOT  


`pip install --upgrade certbot` &nbsp; &nbsp; &nbsp;  install certbot  
`which certbot` &nbsp; &nbsp; &nbsp;  see that certbot is in the path  
`find / -name "certbot"` &nbsp; &nbsp; &nbsp;  find files related to certbot installed  
`pip list | grep cert` &nbsp; &nbsp; &nbsp;  see that certbot is now installed  
`certbot plugins --text`  &nbsp; &nbsp; &nbsp;  see certbot components  
<br/><br/>  
