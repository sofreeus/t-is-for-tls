## LOG FILES  
Normally, certs for all domains get logged in the same /var/log/letsencrypt log file.  
specifying `--logs-dir` as part of the cert creation, creates a seperate log for just that cert, making it easier to parse logs.  

<br/><br/> 

