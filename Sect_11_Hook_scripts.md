## HOOK SCRIPTS
hook scripts are in /srv/certbot/<domain>/hook.sh  
They all essentailly do the same 3 things, which is put the certificate(s) in the needed format, copy the cert(s) to where they need to go, and remote reload/restart service(s) needing to acknowledge the new certificate.  

&nbsp;  _note: `certbot` and hook scripts can be owned and run as non root user._   
&nbsp;  _I like to run as **root**. As non-root user may still needed to ssh/rsync (as root ssh is often disabled), commands are preceeded by_  
&nbsp;  _`su certbot -c `, to indicate a user names certbot that exists on remote servers_  

Each hook script is in the /srv/certbot/<domain> dir (here is an example)  
```

########################################################
## Combine cert + key to pem file in 'live' directory ##
########################################################
cat /etc/letsencrypt/live/domain.net/fullchain.pem > /etc/letsencrypt/live/domain.net/domain.pem
cat /etc/letsencrypt/live/domain.net/privkey.pem >> /etc/letsencrypt/live/domain.net/domain.pem

######################
## Rsync to HAProxy ##
######################
su certbot -c "rsync -azv /etc/letsencrypt/live/domain.net/domain.pem haproxy01.something.net:/etc/ssl/private/"

############################
## Reload HAproxy service ##
############################
su certbot -c "ssh -tt haproxy01.something.net 'sudo systemctl reload haproxy.service'"
```
<br/><br/> 
