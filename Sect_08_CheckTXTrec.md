##  CHECK TXT RECORD IN DNS-01 CHALLENGE  
If for some reason cert creation/renewal fails, it might be helpful to see that the acme DNS challenge is happening.  
Use the `dig` command to the IP of the DNS provider (there may be more than one), and search for an acme-challenge for the domain you are creating the cert.  
This example is searching for a DNS TXT record that should come and go for the zone 'sofree.us'.    
`while true; do dig @<DNS_IP> _acme-challenge.<host>sofree.us TXT +short; sleep 3; done`

<br/><br/>  
