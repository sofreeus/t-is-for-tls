# INSTALL CERTBOT ON UBUNTU 20.04
**certbot** will be installed by the root user, using **pip**  

why not `apt` or `snap`?  
&nbsp; &nbsp; _`snap` created clutter, does not uninstall cleanly, and by default wants to auto-update.  **certbot** package in `apt` repo is far out of date._  


why install as root?  
&nbsp; &nbsp; _certbot has instructions on how to install as non-root, if you would prefer, but there are a lot of work-arounds,_   
&nbsp; &nbsp; _(chmod/chown dirs and files, and adding extra --\<options\> to the certbot command (`--work-dir`, `--logs-dir`, and `--config-dir`), and more)._  
&nbsp; &nbsp; _certbot was designed to run as root user. For troubleshooting & support purposes, I like running as root_    

<br/><br/>  
