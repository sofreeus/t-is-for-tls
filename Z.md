## SFS CERTBOT CLASS STEPS

 `apt upgrade -y`  
   &nbsp; &nbsp; upgrade apt repos before installing packages

 `apt install python3-pip -y`  
   &nbsp; &nbsp; install PIP. 

 `pip --version`  

 `pip install --upgrade pip`  
   &nbsp; &nbsp; update version of PIP (should update one point rev.)  

 `pip list`  
   &nbsp; &nbsp; see that a lot of python packages are avail from the install of PIP.   
   &nbsp; &nbsp; see "zope.interfaces" version. Install of certbot would fail, because it has a dependency on a later version of this Python package.   
   &nbsp; &nbsp; feel free to attempt cetbot install to see the error   

 `pip install --upgrade zope.interface`  
   &nbsp; &nbsp; update version of zope.interface.  
   &nbsp; &nbsp; you may do pip list again to see the version of zope.interface change.  

 `pip install --upgrade certbot`  
  &nbsp; &nbsp; Install certbot.  

 `pip install certbot-plugin-gandi`  
   &nbsp; &nbsp; Install certbot gandi plugin. 

 `pip list | grep cert`  
   &nbsp; &nbsp; See that certbot and the certbot plugin are installed. 
 
 `certbot plugins --text`  
   &nbsp; &nbsp; See all the plugings that certbot is aware of. 

-----

 `mkdir -p /etc/letsencrypt/gandi`  
  &nbsp; &nbsp; make the directoy for the gandi DNS credentials. 

 `vi /etc/letsencrypt/gandi/gandi.ini` &nbsp; &nbsp; (dns_gandi_api_key=ZG5MAxZ06LCLA2VDlCOnyUGc)  
  &nbsp; &nbsp; add API key to .ini file. 
 
 `chmod 600 /etc/letsencrypt/gandi/gandi.ini`  
  &nbsp; &nbsp; change the perm since it's a file with access credentials   

 `certbot certonly --authenticator dns-gandi --dns-gandi-credentials /etc/letsencrypt/gandi/gandi.ini --agree-tos --email erglazier@hotmail.com -d stud<#>cb.sofree.us`  
  &nbsp; &nbsp; create certificate. 

 `cd /etc/letsencrypt/live/stud<#>cb.sofree.us`  

 `cp fullchain.pem /etc/ssl/certs/apache.crt`  

 `cp privkey.pem /etc/ssl/private/apache.key`  
   &nbsp; &nbsp; copy cert files for Apache. 

 `a2ensite`   
   &nbsp; &nbsp;  select default_ssl. 

 `vi /etc/apache2/sites-enabled/default-ssl.conf`  
    &nbsp; &nbsp;  add 2 SSL lines. 

 `apachectl configtest`  
   &nbsp; &nbsp; see that there are no erors in the Apache conf. 

 `a2enmod ssl`  
   &nbsp; &nbsp; enable ssl. 

 `systemctl restart apache2`   
   &nbsp; &nbsp; restart or reload apache service. 
