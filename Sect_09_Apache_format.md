## FORMAT THAT APACHE NEEDS A CERT (this could be any app that has a web server component, like Jenkins)  

see /etc/apache2/sites-available/000-default.conf (assuming the web site still uses the default) for the location Apache looks for certs.  
Apache2 (known as `httpd` on CentOS) needs the public and private key in seperate files.   

&nbsp;  **SSLCertificateFile** is your certificate file (e.g., your fullchain file).   
&nbsp;  **SSLCertificateKeyFile** is your private.key.   

Other web apps might need different format   
<br/><br/>  

&nbsp;  (_`apachectl configtest` can be used to verify that the apache config file is good_)  
&nbsp;  (_before changes to apache can take effect, the service must be reloaded ` systemctl reload apache2`_)  


<br/><br/>  
