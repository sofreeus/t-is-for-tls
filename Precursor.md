## SECTION I - What is the class about?

We will be using a component of Transport Layer Security (TLS) to secure a web page.  

Let’s Encrypt is a well-known open project and nonprofit certificate authority that provides TLS certificates to hundreds of thousands of websites around the world. Let’s Encrypt uses the ACME (Automatic Certificate Management Environment) protocol to verify that one controls a given domain name and to issue a certificate. There are mainly two ways to do that:

HTTP validation  
DNS validation  
Also, there are two types of domain name:  

Fully-qualified domain names  
Wildcard domain names   

TLS/SSL certificates are the fundamental concept of PKI and act as security checkpoints in network communication. 
<br/><br/> 


## SECTION II - What is TLS?

- SSL (Secure Socket Layer) was the precursor to TLS, and was born out the need to secure web communications (HTTP and other web traffic)
- The terms SSL and TLS are used interchangibly.
- The original TLS (1.0) arrived in 1999. 
- TLS stand for Transport Layer Security.  

In this class we will be talking about _confidentiality_ and _encryption_ for HTTP traffic.  

2 things TLS does in relation to HTTP is....  
&nbsp;&nbsp; 1\) confirm identity   
&nbsp;&nbsp; 2\) initiate secure communications    

This is done through the Public Key Infrastructure (PKI) piece of TLS,  
and is what we will be focusing on in this class.  

&nbsp; For more information on PKI - https://sectigo.com/resource-library/rsa-vs-dsa-vs-ecc-encryption   
&nbsp; For more information on the standard format for PKI, X.509 - https://www.ssl.com/faqs/what-is-an-x-509-certificate/   
&nbsp; For more information on TLS - https://en.wikipedia.org/wiki/Transport_Layer_Security   

\--------------------------  



_It's worth mentioning that secure web site communication entails a lot of components related to TLS, but this class we will be focusing on digital public key certificates, which is part of the PKI component of a TLS "cipher suite".
The cipher suite could include various other protocols and ways of exchanging data or securing data, but today will be about public key certificate for a web server, to satisfy that piece of TLS._

_Here are some of the TLS communication components, and what issue they may address, if you want to explore those terms (`highlighted`) on your own. Keep in mind that there are numerous combinations of technologies, versions, lengths, etc., in a TLS cipher suite._

_Clients (browsers) and web servers will negotiate the latest and most secure algorithms and protocols that they both support._

&nbsp;&nbsp;  How do I know you are who you say you are? `PKI`   
&nbsp;&nbsp;  What algorithm was used to create the secure certificate? `RSA`? `ECC`?  
&nbsp;&nbsp;  How will this TLS connection be encrypted? `AES`?  
&nbsp;&nbsp;  If a hash function is needed, what algorithm should be used? `SHA-256`?    

\--------------------------  

 <br/><br/>  

  

## SECTION III - TLS flow and Certificate Authorities

Clients (browser) needs to establish a secure line of communication with a web server. Before other secure channels can be established, the client needs to know that the web site is legitimately who they show to be. 
The client needs some proof of identity, before the rest of TLS communication can comence. 

I could get IDd at the liquor store, and pull out a post-it note that I wrote my name and age on, but the store owner requires a document that society mutually agrees upon as providing accurate and authentic information. In this case we all must agree that the US government will do due-diligence when issuing such a document. 

Likewise, a web site owner could generate and sign their own document to tell a client that they are who they claim to be, but much like the liquor store owner, web browsers and client software are not going to accept this.

A Certificate Authority is like the US goverment, in my scenario. There are companies and organizations that have been vetted and approved to be CAs. How CAs are known to be trusted, and all services a CA provides, are beyond the scope of this class. 

CAs provide the base or initial certificate- the one that can ultimately be trusted. Most specifically, they sign the Public (PKI) certificate, a.k.a an X.509 certficate. Having a certificate signed by a public certificate authority is meaningful for web security. Certificate Authorities may produce several of these certificates, known as Root CAs. Like any other public key certificate, these Root CAs only live for a certain amount of time, so you may see Root CA certificates expire, and new ones be released. Browsers and operating systems like to keep an up-to-date list of Root CAs. 

If a browser talks to a web server, the web server must have a non-expired certificate created by a Certificate Authority that the browser is aware of, and may need the root CA to validate this.  

Certificate Authorities provide a certain level of validation on the web stie purchasing the certificate. It will take some amount of time to get a certificate purchased from a CA, because of validation the CA may do, like doing some level of verification of the entity behind the web site, having contacts within the organization supporting the web site, assurance that the entity legitimately owns the domains for which they are purchasing a certificate, etc. A CA can provide different types for validation, and this validation is what you are paying for. You may purchase certificates that expire in 1 year, 3 years, 5 years, etc.

LetsEncrypt is one such Certificate Authority, and is capable of generating digital certificates, but they only perform one type of validation, DV, and they are only valid for 90 days. More on that later.  
<br/><br/>   


## SECTION IV - LetsEncrypt brief introduction

Let’s Encrypt is an open source Certificate Authority that’s backed by companies such as Cisco, Mozilla, AWS, HAProxy, Red Hat, Chrome, Digital Ocean and many more.

LE does Domain Validation (DV), which is to prove you own the domain you are trying to generate a certificate for. There is no deep probe of the company or business practices, or verifying mailing addresses, etc.

Links for more exploration:  
&nbsp; &nbsp;  Non-profit organization behind LetsEncrypt - https://www.abetterinternet.org/about/  
&nbsp; &nbsp;  Letsencrypt support community - https://community.letsencrypt.org/   
  
Questions to think about:  
  - When to use LE instead of a purchased cert?
  - Possible cons of using a LE cert
  - Why do LE certs expire so soon?  
<br/><br/>  


## SECTION V - ACME protocol brief introduction

ACME stands for Automated Certificate Management Environments, and was developed by LetsEncrypt (Internet Security Research Group) as a way for people to go through the LE certificate validation and renewal process themselves. Certificate generation (Certificate Signing Request), renewal and revocation with other CAs is a more manual process and can't be automated. ACME was designed to give people the capability to do this themselves, in an automated fashion. It automates interactions between certificate authorities and their users' web servers.

As described above, ACME lets the cleint do much of the work of the CA. Another big part of what a CA does is domain validation, which an ACME client also handles

DV type validation can be done several ways from an ACME Challenge (see section below), but in general LetsEncrypt will probe a web site with an HTTP request, or probe a DNS provider with a TXT record. There are other challenge types as well.

Links for more exploration:  
&nbsp; &nbsp;  Examples of validation challenges - https://letsencrypt.org/docs/challenge-types/  
&nbsp; &nbsp;  Examples of DNS providers known to LetsEncrypt - https://community.letsencrypt.org/t/dns-providers-who-easily-integrate-with-lets-encrypt-dns-validation/86438     
<br/><br/>  
 

## SECTION VI - Certbot brief introduction

An ACME client is a program designed to help with LetsEncrypt certificate generation, renewal and propagation. There are several ACME cleint software programs. LetsEncrypt had one of its own, named 'letsencrypt'. later renamed 'certbot' to avoid confusion the CA of the same name. 

(Note: there are now other CAs that support ACME. In-other-words, certbot could be used to generate certs from CAs other than Letsencrypt)

Links for more exploration:  
&nbsp; &nbsp;  Organization behind certbot - https://www.eff.org/  
&nbsp; &nbsp;  Certbot site - https://www.eff.org/pages/certbot  


