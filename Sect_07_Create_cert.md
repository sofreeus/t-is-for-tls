## CREATE CERTIFICATE  

do once without without extra flags to see what you get prompted for  (ie. `--agrees-to` and `--email`)  
the email will be used for renewal notifications and security notices  
<br/>  

### Create cert.  
This will point to the ini file, and it specifies what domain we are creating a cert for.   
You will get asked questions about accepting agreements, proving an email, and providing LE feedback.     
```
certbot certonly --authenticator dns-gandi --dns-gandi-credentials /etc/letsencrypt/gandi/gandi.ini -d stud{?}hap.sofree.us
```
<br/>   

### To create the certificate in the same manner without getting prompted.   
```
certbot certonly --authenticator dns-gandi --dns-gandi-credentials /etc/letsencrypt/gandi/gandi.ini --agree-tos --email erglazier@hotmail.com -d stud{?}hap.sofree.us`
```  

&nbsp;  `--force-renew` (not in the certbot commands, but can be added to create a cert before the expiry)  

&nbsp;  `--cert-name` to ensure the /etc/letsencrypt/{archive,live}/directory name is the same as domain name  

&nbsp;  `--test-cert` (not in the certbot comands, but can be added if you need to test that the certbot command works,  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; but don't want it to go against your )  

&nbsp;  `--renew-hook` will only get kicked off after 1st cert create.  (This flag is depricated and should replaced by scripts in post-deploy folder,  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; and run with `certbot renew`)  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp;  _renew is really creating new cert_  

&nbsp;  `--logdir` without this, all the domain's logs get put into /var/log/letsencrypt, making it hard to sort through.   
&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; This will put each domain's logs into a separate log.  It also removes the need to stdout and stderr redirection in the crontab file.    

&nbsp;  bash `sleep` command is to allow certbot enough time to do the DNS acme challenge.  
<br/><br/>  

&nbsp;  _plugin may have it's own option to wait for propigation. Most default to 120 second, but can be increased. This would be used instead of the above "sleep"_   
<br/><br/>  
