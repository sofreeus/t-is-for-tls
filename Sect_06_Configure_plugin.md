## CONFIGURE CERTBOT PLUGIN  
_certbot_ plugins are for DNS challenges.  
Every plugin will have it's own syntax & configuration specific to it, and have associated documentation, but essentially  
DNS plugins run a program to do the LE DNS challenge for you. You just need to provide the DNS provider API login information in an .ini file, that you will reference in the `certbot` command.   

An .ini file for DNS credentials can reside anywhere, but it's commonly under /etc/letsencrypt.   

&nbsp; _(In the absense of a plugin specific fro your DNS provider, you need to script your own challenge,_  
&nbsp; _that will insert a DNS TXT record, run tests against it, and remove the record._   
&nbsp; _This can be accomplished through certbot hook scripts, but is written by the end user.)_   


For **gandi** plugin, you need to create an .ini file with the API key (provided for you).  

### Create .ini file for gandi  

 &nbsp; &nbsp; `mkdir /etc/letsencrypt/gandi; cd /etc/letsencrypt/gandi`  
 &nbsp; &nbsp; `vi gandi.ini` &nbsp; &nbsp; <-- add  &nbsp; &nbsp;  _dns_gandi_api_key=\<APIKEY\>_ (key will be provided)  
 &nbsp; &nbsp; `chmod 600 /etc/letsencrypt/gandi/gandi.ini`  



<br/><br/>  
