## INSTALL PIP  

`apt upgrade`  &nbsp; &nbsp; &nbsp; update repos  
`apt install python3-pip`  &nbsp; &nbsp; &nbsp; install pip  
`pip --version`  &nbsp; &nbsp; &nbsp; check pip version  
`pip install --upgrade pip`  &nbsp; &nbsp; &nbsp; there may be a slightly later version on pip installed from pip itself  
`pip --version`  &nbsp; &nbsp; &nbsp; check if version changed  
`which pip`  &nbsp; &nbsp; &nbsp; find path to pip  
`pip list`  &nbsp; &nbsp; &nbsp; list all of the python packages that come with pip, and their versions  
`pip list | grep cert`  &nbsp; &nbsp; &nbsp; see that certbot is not a pip package  
 
&nbsp; _note: Ubuntu 20.04 pre-bundled with only python3.  Installing python3-pip will install `pip` and `pip3` under /usr/bin, both of which are pip for python3_    

  
