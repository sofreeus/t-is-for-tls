## AUTO RENEW  
In previous versions of certbot, renew gets kicked off after the first cert creation because of the `--renew-hook` option in the certbot command that gets run by cron every night.  
This still works, but is a deprecated function. `certbot renew` is the prefered way to automatically renew certificates. 

<br/><br/>  
