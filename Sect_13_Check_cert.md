## CHECKING CERTS  


You can use the above log files to check information about certificates.  Below are some other ways to validate certificates.   

Check modifyed date of file on certbot server. This will give you an idea of when the last cert was created.  
&nbsp;  `ls -l /etc/letsencrypt/archive/xxx/cert.pem`   

<br/><br/>   
Look inside cert on certbot or where it was copied. This will give you the date is was created and date it expires.  
&nbsp;  `openssl x509 -in /etc/letsencrypt/archive/xxx/cert.pem -text | grep After` (may need to install openssl)   

<br/><br/>   
_curl_ command (if TLS port is not on 443, curl method wont work)  
&nbsp;  `curl -Iv https:\\serverwherecertisused`  
 
<br/><br/>  
_namp_ command.  `namp` usually has to be installed separately on Linux. namp is a more intrusive command, so you might try `openssl` and `curl` first.  
there is a script for SSL/TLS that comes with nmap, that you specify.  
&nbsp;  `nmap -p 443 --script ssl-cert server_or_IP_wherecertisused`  if port is something other than 443, specify that


<br/><br/>  
